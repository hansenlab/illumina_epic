R_TIMING=_R_CHECK_TIMINGS_=0 /usr/bin/time
R_VER=devel
R_CHECK_ARGS=--timings 
## --no-codoc --no-examples --no-vignettes
## --codoc
R_BUILD_ARGS=--no-build-vignettes
R_INSTALL_ARGS=
PACKAGE_NAME_MANIFEST=IlluminaHumanMethylationEPICmanifest
PACKAGE_NAME_ANNOTATION=IlluminaHumanMethylationEPICanno.ilm10b2.hg19


##############
base_manifest: $(PACKAGE_NAME_MANIFEST) 
ifneq (,$(wildcard $(PACKAGE_NAME_MANIFEST)/DESCRIPTION))
PACKAGE_VERSION_MANIFEST:=$(shell grep ^Version $(PACKAGE_NAME_MANIFEST)/DESCRIPTION | sed 's_Version: __')
endif

build_manifest: base_manifest
	R-$(R_VER) CMD build $(R_BUILD_ARGS) $(PACKAGE_NAME_MANIFEST)

check_manifest: build_manifest
	$(R_TIMING) R-$(R_VER) CMD check $(R_CHECK_ARGS) $(PACKAGE_NAME_MANIFEST)_$(PACKAGE_VERSION_MANIFEST).tar.gz
	$(R_TIMING) R-$(R_VER) CMD BiocCheck $(PACKAGE_NAME_MANIFEST)_$(PACKAGE_VERSION_MANIFEST).tar.gz


install_manifest: build_manifest
	R-$(R_VER) CMD INSTALL $(R_INSTALL_ARGS) $(PACKAGE_NAME_MANIFEST)_$(PACKAGE_VERSION_MANIFEST).tar.gz

system_install_manifest: build_manifest
	Rscript-$(R_VER) -e "{ base::hopkins\$$local.install(\"$(PACKAGE_NAME_MANIFEST)_$(PACKAGE_VERSION_MANIFEST).tar.gz\") }"

##############
base_annotation: $(PACKAGE_NAME_ANNOTATION) 
ifneq (,$(wildcard $(PACKAGE_NAME_ANNOTATION)/DESCRIPTION))
PACKAGE_VERSION_ANNOTATION:=$(shell grep ^Version $(PACKAGE_NAME_ANNOTATION)/DESCRIPTION | sed 's_Version: __')
endif

build_annotation: base_annotation
	R-$(R_VER) CMD build $(R_BUILD_ARGS) $(PACKAGE_NAME_ANNOTATION)

check_annotation: build_annotation
	$(R_TIMING) R-$(R_VER) CMD check $(R_CHECK_ARGS) $(PACKAGE_NAME_ANNOTATION)_$(PACKAGE_VERSION_ANNOTATION).tar.gz
	$(R_TIMING) R-$(R_VER) CMD BiocCheck $(PACKAGE_NAME_ANNOTATION)_$(PACKAGE_VERSION_ANNOTATION).tar.gz

install_annotation: build_annotation
	R-$(R_VER) CMD INSTALL $(R_INSTALL_ARGS) $(PACKAGE_NAME_ANNOTATION)_$(PACKAGE_VERSION_ANNOTATION).tar.gz

system_install_annotation: build_annotation
	Rscript-$(R_VER) -e "{ base::hopkins\$$local.install(\"$(PACKAGE_NAME_ANNOTATION)_$(PACKAGE_VERSION_ANNOTATION).tar.gz\") }"

