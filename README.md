Illumina_EPIC 
-------------

This git repository contains packages supporting the Illumina Human Methylation EPIC array; both a manifest package and an annotation package is contained here.

These packages are now available through Bioconductor version 3.3 (release date April 15th together with R-3.3, before then you will need to use R-3.3 beta), with minfi version 1.17.10 or greater.  Use `read.metharray.exp()` to read EPIC and 450k data files.

Package releases
----------------

Source tarballs for R, built using R-devel. These are being submitted to Bioconductor.

- [IlluminaHumanMethylationEPICmanifest_0.3.0.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICmanifest_0.3.0.tar.gz)

- [IlluminaHumanMethylationEPICanno.ilm10b2.hg19_0.3.0.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICanno.ilm10b2.hg19_0.3.0.tar.gz)


Using minfi
-----------

Full support for EPIC arrays with minfi requires minfi version 1.17.10 or greater.  Use the function `read.metharray.exp()` and friends to read both 450k and EPIC arrays.

Older versions of minfi (versions 1.16.x) supports EPIC arrays (with some limitations), with the following caveat: you usually import 450k data into R using `read.450k.exp()`.  This hard-codes the data object to be a 450k array.  You can use the function for reading EPIC arrays as well, but you will need to post-process it, like below.

```{r}
sheet <- read.450k.sheet("data_files/Demo_Data_EPIC")
epicData <- read.450k.exp(targets = sheet)
epicData@annotation <- c(array = "IlluminaHumanMethylationEPIC", annotation = "ilm10b2.hg19")
```

Old Releases
------------

- [IlluminaHumanMethylationEPICmanifest_0.2.0.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICmanifest_0.2.0.tar.gz)
- [IlluminaHumanMethylationEPICanno.ilmn10b.hg19_0.2.0.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICanno.ilmn10b.hg19_0.2.0.tar.gz)
- [IlluminaHumanMethylationEPICmanifest_0.1.1.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICmanifest_0.1.1.tar.gz)
- [IlluminaHumanMethylationEPICanno.ilmn10.hg19_0.1.1.tar.gz](https://dl.dropboxusercontent.com/u/3447867/IlluminaHumanMethylationEPICanno.ilmn10.hg19_0.1.1.tar.gz)

